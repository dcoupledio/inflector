<?php namespace Decoupled\Core\Inflector;

class Inflector implements InflectorInterface{

    protected $subject;


    /**
     * return subject
     *
     * @return     string  string Subject
     */

    public function __toString()
    {
        return $this->subject;
    }

    /**
     * alias for setSubject
     *
     * @param      string  $subject  The subject
     *
     * @return     Decoupled\Core\Inflector\Inflector (self)
     */

    public function __invoke( $subject )
    {
        return $this->setSubject( $subject );
    }

    /**
     * Sets the subject that will be inflected if no value given 
     * to method, ex: $inflector->setSubject( $string )->asVar()
     *
     * @param      string  $subject  The string to be inflected
     *
     * @return     Decoupled\Core\Inflector\Inflector (self)
     */

    public function setSubject( $subject )
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Gets the subject.
     *
     * @return     string  The subject.
     */

    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * inflects given string as variable
     *
     * @param      string  $string  The string to inflect
     * 
     * @return     string inflected as variable name
     */

    public function asVar( $str = null )
    {
        $str = $this->asClass($str ?: $this->getSubject());

        return lcfirst($str);
    }

    /**
     * inflects given string as a key
     *
     * @param      string  $string  The string
     * 
     * @return     string inflected as key
     */    

    public function asKey( $str = null )
    {
        $str = $str ?: $this->getSubject();

        //replace underscores and hyphens with dots
        $str = str_replace(["_","-"], ".", $str);

        //split by uppercase characters, for camelcase
        $str = preg_split('/(?=[A-Z])/', $str, -1, PREG_SPLIT_NO_EMPTY);

        //join by dots
        $str = strtolower( implode(".", $str) );

        return $str;
    }

    /**
     * { function_description }
     *
     * @param      <type>  $str    The string
     *
     * @return     <type>  ( description_of_the_return_value )
     */

    public function asClass( $str = null )
    {
        $str = $str ?: $this->getSubject();

        $str = str_replace(['_','-', ' '], '.', $str );

        $str = implode( '', array_map('ucfirst', explode(".", $str)));

        return $str;
    }
}