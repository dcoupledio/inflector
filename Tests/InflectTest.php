<?php

require('../vendor/autoload.php');

use phpunit\framework\TestCase;
use Decoupled\Core\Inflector\Inflector;

class InflectTest extends TestCase{

    protected $inflector;

    public function getInflector()
    {
        return $this->inflector ?: $this->inflector = new Inflector();
    }

    public function testCanInflectAsVar()
    {
        $inflect = $this->getInflector();

        $var = $inflect('this.is.a.key')->asVar();

        $this->assertEquals( $var, 'thisIsAKey' );
    }

    public function testCanInflectAsClass()
    {
        $inflect = $this->getInflector();

        $className = $inflect('a_var_name')->asClass();

        $this->assertEquals( 'AVarName', $className );
    }

    public function testCanInflectAsKey()
    {
        $inflect = $this->getInflector();

        $key = $inflect( 'value_name' )->asKey();

        $this->assertEquals( $key, 'value.name' );
    }
}