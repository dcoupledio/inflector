<?php namespace Decoupled\Core\Inflector;

interface InflectorInterface{

    /**
     * return default string format
     *
     * @return     string  String representation of the object.
     */

    public function __toString();

    /**
     * alias for setSubject method
     *
     * @param      string  $subject  The subject
     */

    public function __invoke( $subject );

    /**
     * Sets the defualt string to inflect, when no parameter given
     * to inflect methods
     *
     * @param      string  $string  The string
     */

    public function setSubject( $string );

    /**
     * Gets the subject.
     */

    public function getSubject();

    /**
     * inflects given string as variable
     *
     * @param      string  $string  The string to inflect
     */

    public function asVar( $string = null );

    /**
     * inflects given string as a key
     *
     * @param      string  $string  The string
     */

    public function asKey( $string = null );

    /**
     * inflects given string as class name
     *
     * @param      string  $string  The string
     */

    public function asClass( $string = null );
}